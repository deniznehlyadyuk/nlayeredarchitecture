using System;
using System.ComponentModel.DataAnnotations;

namespace Domain.Abstract
{
    public abstract class BaseEntity : IBaseEntity
    {
        [Key]
        public Guid Id { get; set; }

        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool IsDeleted { get; set; } //TODO: private set yapılabilabilir fakat EfEntityRepositoryBase'in delete metodundan da erişilemez.
    }
}
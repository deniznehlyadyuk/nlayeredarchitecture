using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Abstract;

namespace Domain.Concrete
{
    public class AccommendationPlace : BaseEntity
    {
        [ForeignKey("HotelType")]
        [Required]
        public Guid HotelTypeId { get; set; }
        public HotelType HotelType { get; set; }

        [ForeignKey("Address")]
        [Required]
        public Guid AddressId { get; set; }
        public Address Address { get; set; }

        public string Name { get; set; }
    }
}
using System.ComponentModel.DataAnnotations;
using Domain.Abstract;

namespace Domain.Concrete
{
    public class Address : BaseEntity
    {
        [Required]
        [MaxLength(64)]
        public string Country { get; set; }
        
        [Required]
        [MaxLength(64)]
        public string City { get; set; }
        
        [Required]
        [MaxLength(64)]
        public string State { get; set; }
        
        [Required]
        public short Postcode { get; set; }
        
        [Required]
        public string AddressLine { get; set; }

        public AccommendationPlace AccommendationPlace { get; set; }
    }
}
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Abstract;

namespace Domain.Concrete
{
    public class HotelType : BaseEntity
    {
        [Required]
        [MaxLength(64)]
        public string Name { get; set; }

        public ICollection<AccommendationPlace> AccommendationPlaces { get; set; }
    }
}
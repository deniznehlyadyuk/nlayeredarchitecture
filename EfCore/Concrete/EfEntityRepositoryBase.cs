using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Domain.Abstract;
using Infrastucture.Abstract;
using Microsoft.EntityFrameworkCore;

namespace EfCore.Concrete
{
    public class EfEntityRepositoryBase<TEntity, TContext> : IEntityRepository<TEntity>
        where TEntity: class, IBaseEntity, new()
        where TContext: DbContext
    {
        private readonly DbContext _context;

        public EfEntityRepositoryBase(TContext context)
        {
            _context = context;
        }

        public TEntity Add(TEntity entity)
        {
            entity.Id = Guid.NewGuid();
            entity.CreatedAt = DateTime.Now;
            entity.UpdatedAt = null;

            var entry = _context.Entry(entity);
            entry.State = EntityState.Added;
            _context.SaveChanges();
            return entity;
        }

        public TEntity Update(TEntity entity)
        {
            entity.UpdatedAt = DateTime.Now;
            
            var entry = _context.Entry(entity);
            entry.State = EntityState.Modified;
            _context.SaveChanges();
            return entity;
        }

        public TEntity Get(Guid id)
        {
            var entity = _context.Set<TEntity>().FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
            return entity;
        }

        public TEntity Get(Expression<Func<TEntity, bool>> filter)
        {
            Expression<Func<TEntity, bool>> additionalFilter =
                x => x.IsDeleted == false;
            
            filter = Expression.Lambda<Func<TEntity, bool>>(
                Expression.AndAlso(filter, additionalFilter), filter.Parameters);

            var entity = _context.Set<TEntity>().FirstOrDefault(filter);
            return entity;
        }

        public List<TEntity> GetList(Expression<Func<TEntity, bool>> filter = null)
        {
            if (filter == null)
                return _context.Set<TEntity>().Where(x=>x.IsDeleted == false).ToList();
            
            Expression<Func<TEntity, bool>> additionalFilter =
                x => x.IsDeleted == false;
            
            filter = Expression.Lambda<Func<TEntity, bool>>(
                Expression.AndAlso(filter, additionalFilter), filter.Parameters);
            
            return _context.Set<TEntity>().Where(filter).ToList();
        }

        public void Delete(Guid id)
        {
            var entity = _context.Set<TEntity>().FirstOrDefault(x => x.Id == id);

            if (entity == null)
                return;
            
            entity.IsDeleted = true;
            var entry = _context.Entry(entity);
            entry.State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
using Domain.Concrete;
using Microsoft.EntityFrameworkCore;

namespace Infrastucture.Concrete.EntityFramework.Contexts
{
    public static class TrivagoContextOnModelCreatingExtensions
    {
        public static void ConfigureTrivago(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<HotelType>(x =>
            {
                x.ToTable("hotel_types");
            });
            
            modelBuilder.Entity<Address>(x =>
            {
                x.ToTable("address");
            });
            
            modelBuilder.Entity<AccommendationPlace>(x =>
            {
                x.ToTable("accommendation_places");
            });
        }
    }
}
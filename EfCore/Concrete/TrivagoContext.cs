using Domain.Concrete;
using Microsoft.EntityFrameworkCore;

namespace Infrastucture.Concrete.EntityFramework.Contexts
{
    public class TrivagoContext : DbContext
    {
        public TrivagoContext(DbContextOptions<TrivagoContext> options) :
            base(options)
        {
        }

        public TrivagoContext()
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ConfigureTrivago();
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<HotelType> HotelTypes { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<AccommendationPlace> AccommendationPlaces { get; set; }
    }
}
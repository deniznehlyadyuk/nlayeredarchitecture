﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EfCore.Migrations
{
    public partial class AccommAccommendationPlace_Entity_Changed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "accommendation_places",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "accommendation_places");
        }
    }
}

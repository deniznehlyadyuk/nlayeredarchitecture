using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Domain.Abstract;

namespace Infrastucture.Abstract
{
    public interface IEntityRepository<TEntity>
        where TEntity: class, IBaseEntity, new()
    {
        public TEntity Add(TEntity entity);
        public TEntity Update(TEntity entity);
        public TEntity Get(Guid id);
        public TEntity Get(Expression<Func<TEntity, bool>> filter);
        public List<TEntity> GetList(Expression<Func<TEntity, bool>> filter = null);
        public void Delete(Guid id);
    }
}
using Application.Contract.DTOs;
using AutoMapper;
using Domain.Concrete;

namespace Application.AutoMapperProfiles
{
    public class AddressProfile : Profile
    {
        public AddressProfile()
        {
            CreateMap<AddressDto, Address>().ReverseMap();
        }
    }
}
using Application.Contract.DTOs;
using AutoMapper;
using Domain.Concrete;

namespace Application.AutoMapperProfiles
{
    public class AccommodationPlaceProfile : Profile
    {
        public AccommodationPlaceProfile()
        {
            CreateMap<AccommendationPlaceCreateUpdateDto, AccommendationPlace>().ReverseMap();
        }
    }
}
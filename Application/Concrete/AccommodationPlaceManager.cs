using System;
using System.Collections.Generic;
using Application.Abstract;
using Application.Contract.DTOs;
using AutoMapper;
using Domain.Concrete;
using Infrastucture.Abstract;

namespace Application.Concrete
{
    public class AccommodationPlaceManager : IAccommodationPlaceAppService
    {
        private readonly IAccommedationPlaceDal _accommodationPlaceDal;
        private readonly IAddressDal _addressDal;
        private readonly IMapper _mapper;

        public AccommodationPlaceManager(
            IAccommedationPlaceDal accommodationPlaceDal, 
            IMapper mapper, 
            IAddressDal addressDal)
        {
            _accommodationPlaceDal = accommodationPlaceDal;
            _mapper = mapper;
            _addressDal = addressDal;
        }

        private Address CreateAddress(AddressDto addressDto)
        {
            var address = _mapper.Map<AddressDto, Address>(addressDto);
            var newAddress = _addressDal.Add(address);
            return newAddress;
        }

        private Address GetAddress(Guid addressId)
        {
            var address = _addressDal.Get(addressId);
            return address;
        }

        private void UpdateAddress(Guid addressId, AddressDto addressDto)
        {
            var address = _addressDal.Get(addressId);
            
            address.Country = addressDto.Country;
            address.City = addressDto.City;
            address.State = addressDto.State;
            address.Postcode = addressDto.Postcode;
            address.AddressLine = addressDto.AddressLine;
            
            _addressDal.Update(address);
        }
        
        public AccommendationPlaceCreateUpdateDto Insert(AccommendationPlaceCreateUpdateDto entityDto)
        {
            var entity = _mapper.Map<AccommendationPlaceCreateUpdateDto, AccommendationPlace>(entityDto);
            var address = CreateAddress(entityDto.AddressDto);

            entity.AddressId = address.Id;
            
            entity = _accommodationPlaceDal.Add(entity);

            return Get(entity.Id);
        }

        public AccommendationPlaceCreateUpdateDto Update(Guid id, AccommendationPlaceCreateUpdateDto entityDto)
        {
            entityDto.Id = id;
            
            var accommendationPlace = _accommodationPlaceDal.Get(id);
            UpdateAddress(accommendationPlace.AddressId, entityDto.AddressDto);

            accommendationPlace.HotelTypeId = entityDto.HotelTypeId;
            accommendationPlace.Name = entityDto.Name;
            
            _accommodationPlaceDal.Update(accommendationPlace);

            return Get(id);
        }

        public AccommendationPlaceCreateUpdateDto Get(Guid id)
        {
            var accommendationPlace = _accommodationPlaceDal.Get(id);
            var address = GetAddress(accommendationPlace.AddressId);
            var accommendationPlaceDto =
                _mapper.Map<AccommendationPlace, AccommendationPlaceCreateUpdateDto>(accommendationPlace);
            accommendationPlaceDto.AddressDto = _mapper.Map<Address, AddressDto>(address);
            return accommendationPlaceDto;
        }

        public List<AccommendationPlaceCreateUpdateDto> GetList()
        {
            var accommendationPlaceDtos = new List<AccommendationPlaceCreateUpdateDto>();
            
            var accommendationPlaces = _accommodationPlaceDal.GetList();
            
            foreach (var accommendationPlace in accommendationPlaces)
            {
                var address = GetAddress(accommendationPlace.AddressId);
                var accommendationPlaceDto =
                    _mapper.Map<AccommendationPlace, AccommendationPlaceCreateUpdateDto>(accommendationPlace);
                accommendationPlaceDto.AddressDto = _mapper.Map<Address, AddressDto>(address);
                accommendationPlaceDtos.Add(accommendationPlaceDto);
            }

            return accommendationPlaceDtos;
        }

        public void Delete(Guid id)
        {
            var accommendationPlace = _accommodationPlaceDal.Get(id);
            var addressId = accommendationPlace.AddressId;
            
            _accommodationPlaceDal.Delete(id);
            _addressDal.Delete(addressId);
        }
    }
}
using System;
using System.Collections.Generic;
using Application.Contract.DTOs.Abstract;
using Domain.Abstract;

namespace Application.Abstract
{
    public interface IEntityAppService<TEntityDto>
        where TEntityDto: class, IBaseDto, new()
    {
        public TEntityDto Insert(TEntityDto entityDto);
        public TEntityDto Update(Guid id, TEntityDto entityDto);
        public TEntityDto Get(Guid id);
        public List<TEntityDto> GetList();
        public void Delete(Guid id);
    }
}
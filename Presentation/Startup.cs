using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Abstract;
using Application.AutoMapperProfiles;
using Application.Concrete;
using Application.Contract.DTOs;
using Application.Contract.Validators;
using Autofac;
using AutoMapper;
using FluentValidation.AspNetCore;
using Infrastucture.Abstract;
using Infrastucture.Concrete.EntityFramework.Contexts;
using Infrastucture.EntityFramework.Concrete;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace Presentation
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IAccommodationPlaceAppService, AccommodationPlaceManager>();
            services.AddScoped<IAccommedationPlaceDal, EfAccommedationPlaceDal>();
            services.AddScoped<IAddressDal, EfAddressDal>();
            
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddDbContext<TrivagoContext>(options => 
                options.UseNpgsql("Host=localhost;Port=5432;Database=DDDExample;User ID=postgres;Password=0B861439fCa66;Include Error Detail=true;"));
            
            services.AddMvc(setup =>
            {
            }).AddFluentValidation(fv =>
            {
                fv.ConfigureClientsideValidation(enabled: false);
                fv.ImplicitlyValidateChildProperties = true;
                fv.RegisterValidatorsFromAssemblyContaining<AccommodationPlaceCreateUpdateDtoValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<AddressDtoValidator>();
            });

            services.AddAutoMapper(typeof(AccommodationPlaceProfile));
            services.AddAutoMapper(typeof(AddressProfile));
            
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "Presentation", Version = "v1"});
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Presentation v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}
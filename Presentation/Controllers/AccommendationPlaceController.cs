using System;
using System.Collections.Generic;
using Application.Abstract;
using Application.Contract.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AccommendationPlaceController : ControllerBase
    {
        private readonly IAccommodationPlaceAppService _accommodationPlaceAppService;

        public AccommendationPlaceController(IAccommodationPlaceAppService accommodationPlaceAppService)
        {
            _accommodationPlaceAppService = accommodationPlaceAppService;
        }
        
        [HttpPost]
        public AccommendationPlaceCreateUpdateDto Add(AccommendationPlaceCreateUpdateDto entityDto)
        {
            return _accommodationPlaceAppService.Insert(entityDto);
        }

        [HttpPut]
        public AccommendationPlaceCreateUpdateDto Update(Guid id, AccommendationPlaceCreateUpdateDto entityDto)
        {
            return _accommodationPlaceAppService.Update(id, entityDto);
        }

        [HttpGet("{id:guid}")]
        public AccommendationPlaceCreateUpdateDto Get(Guid id)
        {
            return _accommodationPlaceAppService.Get(id);
        }
        
        [HttpGet]
        public List<AccommendationPlaceCreateUpdateDto> GetList()
        {
            return _accommodationPlaceAppService.GetList();
        }

        [HttpDelete("{id:guid}")]
        public void Delete(Guid id)
        {
            _accommodationPlaceAppService.Delete(id);
        }
    }
}
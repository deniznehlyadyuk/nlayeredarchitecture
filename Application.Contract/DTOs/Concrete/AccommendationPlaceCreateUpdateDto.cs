using System;
using Application.Contract.DTOs.Abstract;

namespace Application.Contract.DTOs
{
    public class AccommendationPlaceCreateUpdateDto : BaseDto
    {
        public Guid HotelTypeId { get; set; }
        public Guid? AddressId { get; set; }
        public AddressDto AddressDto { get; set; }
        public string Name { get; set; }
    }
}
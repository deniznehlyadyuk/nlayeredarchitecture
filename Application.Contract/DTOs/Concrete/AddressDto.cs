namespace Application.Contract.DTOs
{
    public class AddressDto
    {
        public string Country { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public short Postcode { get; set; }
        public string AddressLine { get; set; }
    }
}
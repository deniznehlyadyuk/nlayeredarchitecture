using System;

namespace Application.Contract.DTOs.Abstract
{
    public abstract class BaseDto : IBaseDto
    {
        public Guid Id { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
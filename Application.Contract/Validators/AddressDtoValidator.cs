using Application.Contract.DTOs;
using FluentValidation;

namespace Application.Contract.Validators
{
    public class AddressDtoValidator : AbstractValidator<AddressDto>
    {
        public AddressDtoValidator()
        {
            RuleFor(x => x.Country)
                .NotEmpty()
                .WithMessage(ValidationErrors.Empty)
                .Length(1, 64)
                .WithMessage(ValidationErrors.GenerateLengthError(1, 64));
            
            RuleFor(x => x.City)
                .NotEmpty()
                .WithMessage(ValidationErrors.Empty)
                .Length(1, 64)
                .WithMessage(ValidationErrors.GenerateLengthError(1, 64));
            
            RuleFor(x => x.State)
                .NotEmpty()
                .WithMessage(ValidationErrors.Empty)
                .Length(1, 64)
                .WithMessage(ValidationErrors.GenerateLengthError(1, 64));

            RuleFor(x => x.AddressLine)
                .NotEmpty()
                .WithMessage(ValidationErrors.Empty);
        }
    }
}
namespace Application.Contract.Validators
{
    public static class ValidationErrors
    {
        public const string MustBeNull = "Entity value must be null.";
        public const string Empty = "Entity value cannot be empty.";

        public static string GenerateLengthError(int x1, int x2)
        {
            return $"Entity value length must between {x1} and {x2}";
        }
    }
}
using Application.Contract.DTOs;
using FluentValidation;

namespace Application.Contract.Validators
{
    public class AccommodationPlaceCreateUpdateDtoValidator : AbstractValidator<AccommendationPlaceCreateUpdateDto>
    {
        public AccommodationPlaceCreateUpdateDtoValidator()
        {
            RuleFor(x => x.AddressId)
                .Equal(x => null)
                .WithMessage(ValidationErrors.MustBeNull);

            RuleFor(x => x.AddressDto).SetValidator(new AddressDtoValidator());

            RuleFor(x => x.Name)
                .NotEmpty()
                .WithMessage(ValidationErrors.Empty);
        }
    }
}
using Domain.Concrete;

namespace Infrastucture.Abstract
{
    public interface IAddressDal : IEntityRepository<Address>
    {
        
    }
}
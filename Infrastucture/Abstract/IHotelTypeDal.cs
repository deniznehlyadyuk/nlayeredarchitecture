using Domain.Concrete;

namespace Infrastucture.Abstract
{
    public interface IHotelTypeDal : IEntityRepository<HotelType>
    {
        
    }
}
using Domain.Concrete;
using EfCore.Concrete;
using Infrastucture.Abstract;
using Infrastucture.Concrete.EntityFramework.Contexts;

namespace Infrastucture.EntityFramework.Concrete
{
    public class EfHotelTypeDal : EfEntityRepositoryBase<HotelType, TrivagoContext>, IHotelTypeDal
    {
        public EfHotelTypeDal(TrivagoContext context) : base(context)
        {
        }
    }
}
using Domain.Concrete;
using EfCore.Concrete;
using Infrastucture.Abstract;
using Infrastucture.Concrete.EntityFramework.Contexts;

namespace Infrastucture.EntityFramework.Concrete
{
    public class EfAddressDal : EfEntityRepositoryBase<Address, TrivagoContext>, IAddressDal
    {
        public EfAddressDal(TrivagoContext context) : base(context)
        {
        }
    }
}
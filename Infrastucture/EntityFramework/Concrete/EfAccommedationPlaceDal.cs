using Domain.Concrete;
using EfCore.Concrete;
using Infrastucture.Abstract;
using Infrastucture.Concrete.EntityFramework.Contexts;

namespace Infrastucture.EntityFramework.Concrete
{
    public class EfAccommedationPlaceDal : EfEntityRepositoryBase<AccommendationPlace, TrivagoContext>, IAccommedationPlaceDal
    {
        public EfAccommedationPlaceDal(TrivagoContext context) : base(context)
        {
        }
    }
}